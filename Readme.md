# Phoenix websocket client example

This project is a very basic example which shows how to connect to a Phoenix websocket, in particular the one exposed by Labyrinth.
In `src/index.js` there is the javascript part which connects and subscribes to the socket depending on the user uuid passed by the user.

## How to run the project
Download dependencies with `npm install`.

Build the project with `npm run build`.

Run the project in a http-server.
