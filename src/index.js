import "phoenix_html"
import {Socket} from "phoenix"

//let user = document.getElementById("user").innerText;

let token = document.getElementById("token")
let url = document.getElementById("url")

let userUuid = document.getElementById("userUuid")
let room;

let messages = document.getElementById("messages")
let renderMessage = (message) => {
    messages.innerHTML = `<b>${syntaxHighlight(JSON.stringify(message, null, 2))}</b>`
}

let startButton = document.getElementById("start");

startButton.addEventListener("click", (e) => {
  let socket = new Socket(url.value + "/socket", {params: {token: token.value}})
  socket.connect()

  if (userUuid.value != "") {

    room = socket.channel("session_events:" + userUuid.value, null);
    room.join();
    room.on("session_event_message", message => renderMessage(message))
  }
})

let stopButton = document.getElementById("stop");

stopButton.addEventListener("click", (e) => {
  room.leave();
});

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

function disconnect(session) {
  room.leave()
}
